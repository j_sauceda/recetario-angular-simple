import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { RecetasComponent } from './components/recetas/recetas.component';
import { ArticuloComponent } from './components/articulo/articulo.component';
import { AcercaComponent } from './components/acerca/acerca.component';

const appRoutes: Routes = [
  { path: '', component: RecetasComponent },
  { path: 'acerca', component: AcercaComponent },
  { path: 'articulo/:id', component: ArticuloComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    RecetasComponent,
    ArticuloComponent,
    AcercaComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes, { enableTracing: true }),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
