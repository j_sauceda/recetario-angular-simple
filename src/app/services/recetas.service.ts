import { Injectable } from '@angular/core';
import { recetas } from '../mock-recetas';
import { Receta } from '../Receta';

@Injectable({
  providedIn: 'root',
})
export class RecetasService {
  recetario: Receta[] = recetas;

  constructor() {}

  get_recetas(): Receta[] | undefined {
    return this.recetario;
  }

  get_receta(id: number): Receta | undefined {
    return this.recetario.find((r) => r.id === id);
  }
}
