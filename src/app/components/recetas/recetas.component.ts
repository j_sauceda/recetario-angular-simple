import { Component, OnInit } from '@angular/core';
import { RecetasService } from 'src/app/services/recetas.service';
import { Receta } from 'src/app/Receta';

@Component({
  selector: 'app-recetas',
  templateUrl: './recetas.component.html',
  styleUrls: ['./recetas.component.css'],
})
export class RecetasComponent implements OnInit {
  recetas!: Receta[] | undefined;

  constructor(private recetaService: RecetasService) {
    // this.recetas = this.recetaService.get_recetas();
  }

  ngOnInit(): void {
    this.recetas = this.recetaService.get_recetas();
  }
}
