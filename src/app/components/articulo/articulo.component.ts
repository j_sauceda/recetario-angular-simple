import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RecetasService } from 'src/app/services/recetas.service';
import { Receta } from 'src/app/Receta';

@Component({
  selector: 'app-articulo',
  templateUrl: './articulo.component.html',
  styleUrls: ['./articulo.component.css'],
})
export class ArticuloComponent implements OnInit {
  articulo!: Receta | undefined;
  id_articulo!: number;

  constructor(
    private recetaService: RecetasService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.id_articulo = Number(this.activatedRoute.snapshot.paramMap.get('id'));
    this.articulo = this.recetaService.get_receta(this.id_articulo);
  }
}
