# Recetario Angular - parte 1

Temas:

- Semantic UI
- Services
- Pipes
- Routes & Dynamic Routes

Próximo módulo:

- Promises, callback, async, await
- Node + Express + json-server backend
- Debug
- Unit tests

## Pasos previos

- limpiar app.components.ts:

```javascript
import { Component } from "@angular/core";

@Component({
  selector: "my-app",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent {
  title = "Recetario Angular";
}
```

- preparar app.component.html:

```xml
<div class="ui middle aligned center aligned grid">
  <div class="ui column">
    <h1 class="ui teal header">{{ title }}</h1>
    <!-- Aquí va el blog de recetas -->
    <div class="ui message">
      <a href="#">Acerca de este sitio</a>
    </div>
  </div>
</div>
```

## Semantic UI

- $ npm install semantic-ui-css
- editar styles.css: @import "semantic-ui-css/semantic.css";
- probar app.component.html insertando: <i [ngStyle]="{'color': 'red'}" class="trash alternate icon"></i>
- crear Receta.ts en /app:

```javascript
export interface Receta {
  id: number;
  nombre: string;
  imagen: string;
  introduccion: string;
  ingredientes: string[];
  preparacion: string[];
  consejos?: string[];
  fuente: string;
}
```

- crear mock-recetas.ts en /app:

```javascript
import { Receta } from "./Receta";

export let recetas: Receta[] = [
  {
    id: 1,
    nombre: "Horchata Hondureña",
    imagen:
      "https://www.honduras.com/wp-content/uploads/2020/07/Horchata-1-768x512.jpg",
    introduccion:
      "La horchata hondureña es una bebida a base de arroz que encanta a todos los catrachos. Además de ser muy sabrosa, contiene varias vitaminas que la vuelven una de las recetas favoritas del país. Aquí te compartimos la cómo prepararla con estos sencillos pasos.",
    ingredientes: [
      "1 libra de arroz. La cantidad depende de cuánto se desee preparar",
      "Canela en rajas",
      "1/2 libra de semilla de ayote",
      "Azúcar al gusto",
      "1/4 libra de semilla de morro",
      "1/2 libra de maní",
      "Conchas de limón",
      "Agua purificada",
    ],
    preparacion: [
      "Lavar bien el arroz",
      "Una vez bien lavado, se debe ablanar con agua",
      "Tostar por separado la canela, el morro, el maní y la semilla de ayote en un comal o sartén",
      "Al maní se le debe retirar la concha una vez este",
      "Una vez esté blando el arroz, se debe licuar con los demás ingredientes añadiendo el agua con el que se ablandó anteriormente",
      "El producto se cuela lentamente con una manta agregando agua, hasta obtener una bebida algo espesa",
      "Se agregan las conchas de limón en trozos pequeños",
      "Poner hielo y azúcar al gusto y disfrutar",
    ],
    consejos: [],
    fuente:
      "https://www.honduras.com/aprende/cocina/receta-para-hacer-horchata-hondurena/",
  },
  {
    id: 2,
    nombre: "Feijoada brasileña",
    imagen:
      "https://www.lavanguardia.com/files/image_948_465/files/fp/uploads/2021/03/18/60534473eda1f.r_d.1062-756-4742.jpeg",
    introduccion:
      "La feijoada o frijolada es el plato nacional de Brasil, se trata de un guiso de alubias negras o frijoles y verduras, acompañados de carne de cerdo y embutidos. Recuerda, en su forma de elaboración, a algunos platos tradicionales de nuestra cocina como las fabadas o alubias estofadas.",
    ingredientes: [
      "200 gr de frijoles",
      "125 gr de carne magra de cerdo cortada en tacos",
      "1 chorizo criollo cortado a rodajas",
      "100 gr de panceta de cerdo cortada en tacos",
      "125 gr de costilla de cerdo cortada en trozos",
      "Un diente de ajo",
      "1 cebolla",
      "3 tomates maduros",
      "1 vaso de vino blanco",
      "Una hoja de laurel",
      "Una pizca de sal",
      "Una pizca de pimienta negra molida",
      "Una cucharada de aceite de oliva virgen extra",
      "200 gramos de arroz largo hervido",
    ],
    preparacion: [
      "En una cazuela con un poco de aceite de oliva sofríe todas las carnes que ya tienes cortadas. Una vez estén bien doradas, retíralas de la sartén y resérvalas",
      "En el mismo aceite dora el ajo y la cebolla picados hasta que tengan un color dorada y estén tiernas. Añade el vaso de vino y déjalo reducir completamente",
      "Añade el tomate cortado en trozos irregulares y el laurel y deja cocinar todo el conjunto unos 5 minutos",
      "Añade los frijoles, las carnes y salpimienta. Vierte agua hasta cubrir y deja cocinar a fuego lento 1 hora y media a 2 horas, hasta que los frijoles estén tiernos",
      "Presentación: Coloca un poco de frijol en un lado, las carnes en el otro y la salsa encima. Añade un par de cucharadas de arroz hervido en el lado opuesto y lo tendrás listo para servir",
    ],
    consejos: [
      "Cada vez que arranque el hervor el guiso, vierte un vaso de agua fría. Hazlo 3 veces y conseguirás unos frijoles más tiernos",
      "Puedes hacer el guiso de la misma forma, pero en olla exprés reduciendo el tiempo de cocción a unos 40 minutos, dependiendo del tipo de frijoles",
      "Si te ha quedado un poco caldoso, coge el agua de cocción y tritura algún frijol con la ayuda de una batidora eléctrica para espesar el guiso",
    ],
    fuente:
      "https://www.lavanguardia.com/comer/recetas/20210326/6601773/feijoada-brasil-brasileno-frijol-receta.html",
  },
  {
    id: 3,
    nombre: "Maultaschen alemanes clásicos",
    imagen: "https://img.cocinarico.es/2020-07/muultaschen-clasicos-1.webp",
    introduccion:
      "Los Maultaschen son un plato típico del sur de Alemania, de la región de Suabia. Se trata de un ravioli relleno de verduras y carne. Espectacular",
    ingredientes: [
      "150 g de harina de trigo",
      "6 Cebollines",
      "2 Pastillas de de caldo de carne",
      "3 Salchichas Bratwurst frescas",
      "3 Huevo",
      "1 Puerro",
      "1 Cebolla",
      "1 Diente de ajo",
      "4 cucharadas de pan rallado",
      "2 cucharadas de agua",
      "1 cucharada de aceite de oliva",
      "1/2 cucharadita de sal",
      "1 puñado de perejil",
      "Pimienta al gusto",
    ],
    preparacion: [
      "En un bol grande, agrega la harina, 1 huevo, la sal, el aceite y el agua. Amasa bien. Si es necesario añade un poco de agua, hazlo. Debe quedar una masa lisa y no pegarse en el bol. Tapa el bol y déjalo reposar",
      "Pica la cebolla y el ajo. Rehógalos con un poco de aceite y déjalos enfriar",
      "Pica el cebollino y el perejil. Ponlos en un bol grande. Quita la tripa de las salchichas y desmenúzalas. Agrega el pan rallado, el huevo y una pizca de sal. Agrega la cebolla y el ajo ya rehogados y enfriados. Mézclalo y reserva",
      "En una cazuela grande, agrega 4 litros de agua y desmenuza las pastillas de caldo de carne. Llévalo a ebullición",
      "Con ayuda de un rodillo, estira la masa sobre una superficie enharinada dándole forma alargada con un ancho de unos 15 cm. Coloca el relleno a lo largo en el centro. Bate un huevo y pinta los bordes de la masa. Cierra la masa. Empieza por los extremos hacia dentro y luego los lados más largo, formando una especie de rollito gigante. Con el palo de de una cuchara de madera marca los raviolis de aproximadamente 9 centímetros. Córtalos con un cuchillo",
      "Cuece los Maultaschen durante 8 minutos en el caldo. Sírvelos con un poco de caldo",
      "Ya están listos los Maultaschen clásicos",
    ],
    consejos: [],
    fuente: "https://www.cocinarico.es/receta/maultaschen-clasicos",
  },
  {
    id: 4,
    nombre: "Pastel Lamington australiano",
    imagen: "https://i.blogs.es/39a795/lamingtonss/1366_2000.jpg",
    introduccion:
      "El barón Lamington fue el gobernador de Queenslad, Australia, entre 1896 y 1901. Aunque sabemos que estos pastelitos llevan su nombre, por eso se llaman Lamingtons, no queda claro a quién se le ocurrió la idea de bañar dados de bizcocho en chocolate y rebozarlos con coco rallado, pero lo cierto es que están deliciosos. Son dulces habituales de las tiendas y supermercados de Australia y Nueva Zelanda, aunque como ocurre con las recetas tradicionales, los mejores son siempre los caseros. Su preparación es sencilla, aunque el proceso de rebozado es un poco tedioso así que os aconsejo que os lo toméis con calma, porque vale la pena.",
    ingredientes: [
      "125 g Mantequilla a temperatura ambiente, y un poco más para engrasar el molde (para el bizcocho)",
      "150 g Azúcar molido (para el bizcocho)",
      "2 Huevos (para el bizcocho)",
      "230 g Harina (para el bizcocho)",
      "2 cucharaditas Levadura química (para el bizocho)",
      "1 pizca de sal (para el bizcocho)",
      "1 cucharadita Extracto de vainilla (para el bizcocho)",
      "100 ml Leche (para el bizcocho)",
      "200 g Coco rallado (para rebozar)",
      "400 g Azúcar molido (para la cobertura)",
      "4 cucharadas Cacao en polvo (para la cobertura)",
      "25 g Mantequilla (para la cobertura)",
      "100 ml Leche (para la cobertura)",
    ],
    preparacion: [
      "Precalentamos el horno a 180ºC. Batimos la mantequilla con el azúcar, hasta que la mezcla sea pálida y esponjosa. Añadimos los huevos de uno en uno, batiendo bien después de añadir cada uno. Tamizamos la harina, la sal y la levadura en un bol",
      "En una jarra mezclamos el extracto de vainilla con la leche. Añadimos la mitad de la mezcla de harina a la mezcla de huevos y seguidamente la mitad de la mezcla de leche. Removemos suavemente con una cuchara de madera, añadimos el resto de mezcla de harina y de leche y removemos",
      "Vertemos en un molde cuadrado de 20x20 cm, previamente engrasado y horneamos unos 30 minutos o hasta que pinchando con un palillo este salga limpio. Sacamos del horno y dejamos enfriar sobre una rejilla. Cuando esté totalmente frío, lo partimos en 16 trozos",
      "Para preparar la cobertura, tamizamos el azúcar molido y el cacao en un bol grande. Añadimos la mantequilla y la leche y mezclamos bien hasta obtener una cobertura suave y ligeramente líquida con la que cubrir los dados de bizcocho. Por último, los rebozamos en coco y servimos",
    ],
    consejos: [],
    fuente:
      "https://www.directoalpaladar.com/postres/lamingtons-receta-de-postre",
  },
];
```

## Services

- generar componente recetas: $ ng generate component components/recetas
- editar recetas.component.html:

```xml
<div class="ui centered middle aligned selection list"></div>

<!-- div class="ui centered link cards">
</div -->
```

- generar servicio receta: $ ng g service services/receta
- en /services definir receta.service.ts:

```javascript
import { Injectable } from "@angular/core";
import { Receta } from "../../Receta";
import { recetas } from "../../mock-recetas";

@Injectable({
  providedIn: "root",
})
export class RecetaService {
  recetario: Receta[] | null = recetas;

  constructor() {}

  get_recetas(): Receta[] {
    return this.recetario;
  }

  get_receta(id: number): Receta | null {
    return this.recetario.find((r) => r.id === id);
  }
}
```

- en recetas.component.ts importar Receta.ts y mock-recetas.ts:

```javascript
import { Component, OnInit } from  '@angular/core';
import { RecetaService } from  './services/receta.service';
import { Receta } from  '../Receta';
...
export  class  AppComponent {
	recetas!: Receta[];

	constructor(private  recetaService: RecetaService) {}

	ngOnInit(): void {
		this.recetas = this.recetaService.get_recetas();
	}
}
```

- editar recetas.component.html:

```xml
<div class="ui centered middle aligned selection list">
  <div *ngFor="let i of recetas" class="item">
    <img class="ui avatar image" src="{{ i.imagen }}" alt="imagen" />
    <div class="content">
      <div class="header">{{ i.nombre }}</div>
    </div>
  </div>
</div>

<!-- <div  class="ui centered link cards">
	<div  *ngFor="let i of recetas" class="card">
		<div  class="ui image">
			<img  src="{{ i.imagen }}" />
		</div>
		<div  class="content">
			<div  class="header">{{ i.nombre }} -- Pipes -- </div>
			<div  class="description">
				{{ i.introduccion }}
			</div>
		</div>
	</div>
</div> -->
```

- editar app.component.html:

```xml
<div class="ui middle aligned center aligned grid">
  <div class="ui column">
    <h1 class="ui teal header">{{ title }}</h1>
    <app-recetas></app-recetas>
    <div class="ui message">
      <a href="#">Acerca de este sitio</a>
    </div>
  </div>
</div>
```

- crear componente articulo: $ng g component components/articulo
- editar articulo.component.ts:

```javascript
import { Component, OnInit, Input } from '@angular/core';
import { RecetaService } from  "../../services/receta.service";
import { Receta } from  '../../Receta';

@Component({
	selector: 'app-articulo',
	templateUrl: './articulo.component.html',
	styleUrls: ['./articulo.component.css'],
})
export class  ArticuloComponent  implements  OnInit {
	articulo!: Receta;
	id_articulo: number;

	constructor(private recetaService: RecetaService) {}

	ngOnInit() {
		this.id_articulo = 1;
		this.articulo  =  this.recetaService.get_receta(this.id_articulo);
	}
}
```

- editar articulo.component.html:

```xml
<div *ngIf="articulo" class="card">
  <h2 class="header">{{ articulo.nombre | titlecase }}</h2>

  <div class="ui centered medium image">
    <img src="{{ articulo.imagen }}" />
  </div>

  <div class="ui divider"></div>

  <div class="content">
    <h2 class="ui header">
      <i [ngStyle]="{ color: 'gray' }" class="newspaper outline icon"></i>

      Introducción
    </h2>

    <div class="ui segment content">{{ articulo.introduccion }}</div>

    <h2 class="ui header">
      <i [ngStyle]="{ color: 'gray' }" class="edit outline icon"></i>

      Ingredientes
    </h2>

    <div class="ui segment bulleted list">
      <div *ngFor="let i of articulo.ingredientes" class="item">{{ i }}</div>
    </div>

    <h2 class="ui header">
      <i [ngStyle]="{ color: 'gray' }" class="utensils icon"></i>

      Preparación
    </h2>

    <div class="ui segment bulleted list">
      <div *ngFor="let i of articulo.preparacion" class="item">{{ i }}</div>
    </div>

    <h2 *ngIf="articulo.consejos.length > 0" class="ui header">
      <i [ngStyle]="{ color: 'gray' }" class="utensil spoon icon"></i>

      Consejos
    </h2>

    <div *ngIf="articulo.consejos.length > 0" class="ui segment bulleted list">
      <div *ngFor="let i of articulo.consejos" class="item">{{ i }}</div>
    </div>

    <h2 class="ui header">
      <i [ngStyle]="{ color: 'gray' }" class="globe icon"></i>

      Referencias
    </h2>

    <div class="content">{{ articulo.fuente }}</div>

    <div class="ui divider"></div>
  </div>
</div>

<div class="message">
  <div class="ui message"><a routerLink="/">Página Principal</a></div>
</div>
```

## Pipes

- uso de Pipes: https://docs.angular.lat/guide/pipes
- titleCasePipe: https://docs.angular.lat/api/common/TitleCasePipe
- all pipes: https://docs.angular.lat/api/common#pipes
- editar recetas.component.html:

```xml
<div class="header">{{ i.nombre | titlecase }}</div>
```

## Routes

- crear componente acerca: $ ng g component components/acerca
- editar acerca.component.html:

```xml
<div class="message">
  <div class="ui message">
    <p>Recetario Angular se encuentra en la versión 1.0.0 &copy; 2022</p>
    <a href="#">Página Principal</a>
  </div>
</div>
```

- editar app.module.ts:

```javascript
...
import { RouterModule, Routes } from  '@angular/router';
// import components
...
const  appRoutes: Routes = [
	{path: '', component: AppComponent },
	{path: "acerca", component: AcercaComponent },
	{path: 'articulo', component: RecetaComponent },
];
@NgModule({
	imports: [
		BrowserModule,
		FormsModule,
		RouterModule.forRoot(routes, {enableTracing:  true})
	],
...
```

- editar app.component.html:

```xml
...
<!-- Aquí va el blog de recetas -->
<router-outlet></router-outlet>

<div class="ui message">
  <a routerLink="/acerca">Acerca de este sitio</a>
</div>
...
```

- editar recetas.component.html:

```xml
...
<div class="ui centered link cards">
  <div *ngFor="let i of recetas" class="card">
    <a routerLink="/articulo">
      <div class="ui image">
        <img src="{{ i.imagen }}" />
      </div>
      <div class="content">
        <div class="header">{{ i.nombre | titlecase }}</div>
        <div class="description">{{ i.introduccion }}</div>
      </div>
    </a>
  </div>
</div>
```

- editar app.module.ts y agregar DynamicRoutes:

```javascript
const routes: Routes = [
  ...{ path: "articulo/:id", component: ArticuloComponent },
];
```

- editar recetas.component.html:

```xml
... <a routerLink="/articulo/{{ i.id }}"> ...</a>
```

- editar articulo.component.ts:

```javascript
import { ActivatedRoute } from "@angular/router";
...
export class ArticuloComponent implements OnInit {
	constructor(
		private recetaService: RecetaService,
		private activatedRoute: ActivatedRoute
	) {}

	ngOnInit():  void  {
		this.id_articulo  =  Number(this.activatedRoute.snapshot.paramMap.get("id"));
		this.articulo  =  this.recetaService.get_receta(this.id_articulo);
	}
}
```
